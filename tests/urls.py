from django.urls import path

from tests import views

urlpatterns = [
    path('complete-test/<int:test_id>/', views.complete_test_view, name='complete_test'),
]