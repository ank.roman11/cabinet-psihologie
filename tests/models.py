from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Test(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return f'{self.title}'


class Question(models.Model):
    title = models.CharField(max_length=100)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.title}'


class Answer(models.Model):
    ANSWERS = (('1', 'deloc'), ('2', 'putin'), ('3', 'destul'), ('4', 'foarte mult'))
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.CharField(choices=ANSWERS, max_length=20)
    date_time = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.answer} {self.question}'

