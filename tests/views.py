from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from tests.forms import AnswerForm
from tests.models import Question, Answer


def complete_test_view(request, test_id):
    if request.method == 'POST':
        results = dict(request.POST)
        questions = results['question']
        answers = results['answer']
        for i, question_id in enumerate(questions):
            answer = answers[i]
            current_user = User.objects.get(id=1)
            Answer.objects.create(user=current_user, question_id=question_id, answer=answer)
    questions = Question.objects.filter(test_id=test_id).order_by('-id')
    questions_with_forms = list(
        map(lambda question: {'question': question, 'answer_form': AnswerForm(initial={'question': question})},
            questions))
    return render(request, 'complet_test.html', {'questions_with_forms': questions_with_forms})

