from django import forms
from tests.models import Answer


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['question', 'answer']
        widgets = {
            'question': forms.Select(attrs={'disabled': '', 'class': 'form-control'}),
            'answer': forms.Select(attrs={'class': 'form-control'})

        }
