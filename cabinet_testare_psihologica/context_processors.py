from tests.models import Test


def get_all_tests_per_user(request):
    return {'all_tests': Test.objects.all()}