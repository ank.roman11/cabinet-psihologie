from django import forms
from django.forms import TextInput
from programeaza.models import Programeaza


class ProgrameazaForm(forms.ModelForm):
    class Meta:
        model = Programeaza
        fields = ['first_name', 'last_name', 'email', 'data']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}),
            'data': TextInput(attrs={'class': 'form-control', 'type': 'datetime-local'}),
        }