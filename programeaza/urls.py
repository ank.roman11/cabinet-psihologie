from django.urls import path

from programeaza import views

urlpatterns = [
    path('programeaza-te/', views.ProgrameazaCreateView.as_view(), name='programeaza_te')
]