from django.core.mail import send_mail
from django.views.generic import CreateView
from programeaza.forms import ProgrameazaForm
from programeaza.models import Programeaza


class ProgrameazaCreateView(CreateView):
    template_name = 'programeaza/create.html'
    model = Programeaza
    form_class = ProgrameazaForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = ProgrameazaForm(self.request.POST)
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.save()
            text = f'Buna ziua, v-ati programat astazi la data de {form.cleaned_data["data"]} pentru' \
                   f' {form.cleaned_data["first_name"]} {form.cleaned_data["last_name"]} '
            send_mail('Programarea ta', text, 'myemail@myemail.ro', (str(form.cleaned_data["email"]),), False)
        return super().post(form)
