from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, Textarea, Select, FileInput

from contact.models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'full_name': TextInput(attrs={'placeholder': 'Please enter your full name', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}),
            'subject': TextInput(attrs={'placeholder': 'Please enter your subject', 'class': 'form-control'}),
            'message': Textarea(attrs={'placeholder': 'Please enter your message', 'class': 'form-control'}),
        }