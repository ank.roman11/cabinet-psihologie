from django.urls import reverse_lazy
from django.views.generic import CreateView

from contact.models import Contact
from contact.forms import ContactForm


class ContactCreateView(CreateView):
    template_name = 'contact/create_contact.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('create_contact')

    def post(self, request, *args, **kwargs):
        form = ContactForm(self.request.POST)
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.save()
        return super().post(form)

    def form_invalid(self, form):
        return super().form_invalid(form)