from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from contact import views

urlpatterns = [
    path('contact/', views.ContactCreateView.as_view(), name='create_contact'),
]
