from django.contrib.auth.models import User
from django.db import models


class Contact(models.Model):
    full_name = models.CharField(max_length=30)
    subject = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    message = models.TextField()

    def __str__(self):
        return self.full_name
